# Installationsanleitung #
*(für Linux / Ubuntu)*

## Systemanforderungen: ##
- Python 2.7

Diese Anleitung wurde mit Ubuntu 16.04 LTS getestet und funktioniert wahrscheinlich auf allen aktuellen Debian/Ubuntu 
basierten Distributionen. Die Scripte können prinzipiell auch auf anderen Linux-Distributionen, Mac und Windows laufen, jedoch 
weicht die Installation von den u.g. Schritten ab. 

Um dieses komplexere Project auf einen odere mehrere Server auszurollen wird das fabric Framework verwendet. Es wird von einem Arbeits-PC aus gestartet und führt von dort aus alle notwendigen Installationsschritte auf den Servern aus.

### Installationsablauf (lokaler Arbeits-PC) ###
1. Potentielle Probleme mit "locale" auflösen (wenn von einem nicht-englishen System eingeloggt wird):

	> `sudo apt-get install language-pack-id`

	> `sudo dpkg-reconfigure locales`

2. In das Verzeichnis des Projekts wechseln (wo diese Datei liegt)

3. Installationsscript starten, bei Bedarf die angefragten Eingaben tätigen:

	> `./install_fabric.sh`

### Installationsablauf (auf die Zielserver, mittels Fabric) ###

1. In das Verzeichnis des Projekts wechseln (wo diese Datei liegt)

2. Die Einstellungen in der Stage-Defintionsdatei `deploy/stages/production.py` anpassen (entsprechend der dort hinterlegten Kommentare)

Besonders wichtig ist, in `production.py` die verwendeten Serveradressen einzutragen und sicherzustellen, dass:

* auf die Server mittels SSH eingeloggt werden kann, entweder indem Nutzer und Password eingetragen werden oder mittels SSH-Keys Zugriff möglich ist
* der verwendete SSH-Account entweder über sudo Rechte verfügt oder selber root ist

3. die Installation starten mittels ```fab production deploy```

Sobald die Installation ohne Fehler durchgelaufe ist, ist das System einsatzbereit


# Recommendations for production use #

## Use Proxy Servers ##

Für höhere Geschwindigkeit und bessere Tarnung empfiehlt sich die Verwendung guter Proxy-Server.

Meine bevorzugte Quelle für Proxy Server ist blazingseollc.com, Ich benutze sie schon seit Jahre und habe sie unzähligen Kunden empfohlen. Deshalb hat blazingseo mir inzwischen dieses exklusive Angebot gemacht:

Wenn sie durch diesen Spezial-Link bestellen https://billing.blazingseollc.com/hosting/aff.php?aff=535&to=https://blazingseollc.com/proxy/pricing/ und RUEDIGER als Coupon-Code eingeben, erhalten sie einen 5% Preisnachlass. Ich erhalte dann ebenfalls eine kleine Komission - eigentlich nicht nennenswert, aber so haben sie dieses Angebot strukturiert.

Wenn das zu kompliziert ist, können sie natürlich auch deren Standard-Paket zum Standard-Preis ordern: http://blazingseollc.com/ (oder einen anderen Proxy-Anbieter nutzen).

## Monitoring ##

Da das System für lange Zeit (Monate, Jahre, ...) laufen soll, ist es bewährte Vorgehensweise, das System zu ihrem Infrastruktur-Monitoring hinzuzufügen. 
Meine Empfehlung: tun sie das und monitoren Sie mindestens die Basis-Parameter RAM, CPU und Plattenplatz. Darüberhinaus ist auch das Monitoring von System und Scraper-Logs sinnvoll.

Falls sie das System nicht zu ihrem Monitoring hinzufügen können oder wollen, sprechen Sie mich doch auf meine Wartungs-Pakete an. Diese enthalten neben detailliertem Monitoring auch Bereitschaft meinerseits um im Notfall schnell und unkompliziert Probleme zu beheben.

# Support / Im Fall von Problemen #

Falls Probleme auftreten, können Sie mich per E-Mial erreichen: ruediger.schmidt@gmx.net