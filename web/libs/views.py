# -*- coding: utf-8 -*-
import os
from flask import Flask, render_template, request, flash
import flask_admin
from flask_admin import Admin

import tools
from forms import UploadPool, ScraperForm


def upload_download_view(app, project_name):
    admin = Admin(app,
                  index_view=flask_admin.AdminIndexView(
                      template='upload_download_ui/home.html',
                      url='/'),
                  base_template='upload_download_ui/layout.html',
                  template_mode='bootstrap3', name=project_name)

    admin.add_view(create_upload_view('Upload', 'upload'))
    admin.add_view(create_download_view('Download', 'download'))

    return None

def create_upload_view(name, dirname, category=None):
    view = create_file_access_view(name, dirname, category=category)
    view.can_upload = True
    return view

def create_download_view(name, dirname, category=None):
    view = create_file_access_view(name, dirname, category=category)
    view.can_upload = False
    return view

def create_file_access_view(name, dirname, category=None):
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..', '..', 'shared', 'working_directories', dirname))
    if not os.path.isdir(path):
        os.makedirs(path)

    view = UploadPool(path, name=name, endpoint=dirname, category=category)
    view.allowed_extensions = {'xlsx', 'csv', 'xls'}
    view.can_mkdir = False
    view.can_rename = False
    view.can_upload = False
    return view


def google_web_view(app):
    @app.route('/', methods=['GET', 'POST'])
    def index():
        scraper_form = ScraperForm()

        is_success = False        
        if scraper_form.validate_on_submit():
            response = tools.activate_scraper(request.form['url'])            
            flash(response.get('message'), 'success' if response.get('success') else 'error')
            is_success = response.get('success')

        return render_template('google_ui/index.html', 
            form=scraper_form, 
            url=request.form.get('url') if is_success else None)


def error_view(app):
    """
    Register 0 or more custom error pages (mutates the app passed in).

    :param app: Flask application instance
    :return: None
    """

    def render_status(status):
        """
         Render a custom template for a specific status.
           Source: http://stackoverflow.com/a/30108946

         :param status: Status as a written name
         :type status: str
         :return: None
         """
        # Get the status code from the status, default to a 500 so that we
        # catch all types of errors and treat them as a 500.
        code = getattr(status, 'code', 500)
        return render_template('errors/{0}.html'.format(code)), code

    for error in [404, 500]:
        app.errorhandler(error)(render_status)

    return None