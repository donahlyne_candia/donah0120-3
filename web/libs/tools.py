# -*- coding: utf-8 -*-
import pandas as pd
import os
import logging
import tempfile
import json
import datetime
from dateutil.parser import parse
import requests
import logging
import subprocess
import psutil
import logging
from logging.handlers import RotatingFileHandler

def activate_scraper(start_url):
    if is_scraper_running():
        return _build_scraper_response(True, "Scraper is already running")

    response = project_start_request(start_url)
    if response.get('status') == 'ok':
        return _build_scraper_response(True, "Scraper has started")
    else:
        return _build_scraper_response(False, response.get('message'))

def project_start_request(start_url):
    feed_uri = os.path.join('/usr/local/web/shared/items', 'example', 'spider', create_temp_name())
    data = [
      ('project', 'example'),
      ('spider', 'spider'),
      ('start_url', start_url),
      ('setting', 'FEED_URI=' + feed_uri),
      ('setting', 'FEED_FORMAT=json')
    ]
    response = requests.post('http://localhost:6800/schedule.json', data=data)
    return json.loads(response.text)

def is_scraper_running():
    response = requests.get('http://localhost:6800/listjobs.json?project=example')
    data = json.loads(response.text)
    if data['status'] == 'ok':
        return len([x for x in data['running'] if x['spider'] == 'spider']) > 0
    else:
        return False

def stop_crawling(type, scraping_logs_path):
    try:
        result = json.load(open(scraping_logs_path))
    except:
        result = {}
    try:
        for proc in psutil.process_iter():
            if "scrapy" in proc.name() and type in " ".join(proc.cmdline()):
                proc.terminate()

        result['percentage'] = 100
        result['scraper_status'] = 'stopped'
    except Exception as e:
        result['status'] = "error"
        result['error'] = 'Encountered error while trigger command: ' + str(e)

    return result

def extract_json_dict_child_value(json, path):
    old_json = json

    for x in path:
        if json:
            json = json.get(x)
            if not json:
                break

    return None if old_json == json else json

def parse_to_date(str_date):
    date = None
    try:
        date = parse(str_date)
    except Exception as e:
        pass
    return date

def create_temp_name():
    name = datetime.datetime.now().strftime("%Y_%m_%d_%H%M%S")
    return name + '.csv'

def exception_handler(app, project_name):
    """
    Register 0 or more exception handlers (mutates the app passed in).

    :param app: Flask application instance
    :return: None
    """
    logfilename = '/var/log/web/{0}_{1}.log'.format(project_name, datetime.datetime.now().strftime("%Y_%m_%d"))
    logHandler = RotatingFileHandler(logfilename, maxBytes=1000, backupCount=1)

    logHandler.setLevel(logging.ERROR)
    logHandler.setFormatter(logging.Formatter("""
    Time:               %(asctime)s
    Message type:       %(levelname)s


    Message:

    %(message)s
    """))
    app.logger.addHandler(logHandler)

    return None

def _build_scraper_response(success, message):
    return {'success': success, 'message': message}
